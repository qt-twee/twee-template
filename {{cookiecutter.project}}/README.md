Twee : {{cookiecutter.project}}
===============================

{{cookiecutter.project}} is a Python package that provides an extention to the [Qt-Twee](https://pypi.org/project/Qt-Twee/ "Python Package Index : Qt-Twee framework for Python") project, a framework based upon [Qt](https://www.qt.io/ "Qt : The GUI tool kit").

Install
-------

One may readily install the package using pip, the Python package manager, as follows :

    pip install Qt-Twee
    pip install Twee-{{cookiecutter.project}}

Neither Qt-Twee nor Twee-{{cookiecutter.project}} should enforce the installation of a Qt binding; By design both packages should depend only upon [QtPy](https://pypi.org/project/QtPy/ "Python Package Index : QtPy a wrapper for the Qt Python bindings PyQt4, PyQt5, PySide and PySide2  "), a wrapper around such bindings Qt Bindings.
Thus the user may have to select and install a Qt binding themselves.
Historically a number of Qt bindings for Python have been available includeing [PyQt4](https://pypi.org/project/PyQt4/ "Python Package Index : PyQt4 bindings for Qt 4.XX") and [PyQt5](https://pypi.org/project/PyQt5/ "Python Package Index : PyQt5 bindings for Qt 5.XX") by riverbank and Qt for Python by Qt themselves. 
Qt for Python originated from an open source project called [PySide](https://pypi.org/project/PySide2/ "Python Package Index : PySide bindings for Qt 4.XX") which evolved to become [PySide2](https://pypi.org/project/PySide2/ "Python Package Index : PySide2 bindings for Qt 5.XX").
PySide2 looks to be the defacto project going forwards and users are advised to install this package, over the alterntaives, should they require a Python binding for Qt.

    pip install PySide2

Usage
-----

Once installed Twee-{{cookiecutter.project}} should be directly acessible via subcommand from the command line.

    twee {{cookiecutter.project}} --help

At the discretion of the package authors it may also be possible to invoke the package directly aswell.

    {{cookiecutter.project}} --help

Uninstall
---------

To remove the project from ones machine one may simply invoke pip directly as follows :

    pip uninstall Twee-{{cookiecutter.project}}
    pip uninstall Twee

Similarly one may remove the Qt binding they installed aswell should it no longer be required.

Template
--------

{{cookiecutter.project}} was initially created from the [cookie cutter](https://pypi.org/project/cookiecutter/ "Python Package Index: Cookie Cutter for package templates") [template](https://gitlab.com/manaikan-open-source/twee-template "The Twee-Template repository") provided by the [Qt-Twee](https://gitlab.com/manaikan-open-source/qt-twee "The Qt-Twee project repository") project.
Similarly those wishing to extend the Twee project may create their own extention from the command line as follows :

    pip install Qt-Twee  
    twee-template PROJECT
