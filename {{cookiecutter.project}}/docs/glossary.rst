--------
Glossary
--------

.. glossary ::

   :abbr:`GUI (Graphical User Interface)` : GUI
     An interface that allows users interact graphically with a machine during the operation of an application.

   Qt-Twee : Qt-Twee
     A Python framework for Qt bindings that boths supports and relies upon this package.
