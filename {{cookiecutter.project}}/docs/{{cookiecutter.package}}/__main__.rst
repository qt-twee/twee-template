====
Main
====

.. contents :: {{cookiecutter.package}}.__main__
  :depth: 2


.. automodule :: {{cookiecutter.package}}.__main__
  :members:         
  :undoc-members:   
  :show-inheritance:
