======
Window
======

.. contents :: {{cookiecutter.package}}.window
  :depth: 2


.. automodule :: {{cookiecutter.package}}.window
  :members:         
  :undoc-members:   
  :show-inheritance:

