========================
{{cookiecutter.package}}
========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:
   
   {{cookiecutter.package}}/*

.. contents :: {{cookiecutter.package}}
  :depth: 2


.. automodule :: {{cookiecutter.package}}.__init__
  :members:         
  :undoc-members:   
  :show-inheritance:
