-----
Usage
-----

{{cookiecutter.product}} may be invoked through Qt-Twee via the subcommand ::

    twee {{cookiecutter.package}}
