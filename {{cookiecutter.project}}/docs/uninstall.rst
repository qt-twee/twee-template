--------------
Uninstallation
--------------

{{cookiecutter.project}} may be uninstalled using pip ::

    pip uninstall {{cookiecutter.product}}
