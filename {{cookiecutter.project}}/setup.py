"""
Develop
-------

To setup a development installation one may call either pip as follows ::

   pip install -e .
 
or execute the setup script directly. ::

   python setup.py develop

The pip version is generally preferred.

Distribution
------------

The package may built as a source districution with the following command.
::

  python setup.py sdist

Once ready for distribution it may be uploaded to PyPI using Twine.
::

  twine upload dist/*

Documentation
-------------

Documentation may be generated through the setup script.
::

   python setup.py build_sphinx -b singlehtml|html|latex

Select between HTML,  singleHTML and LaTeX as necessary.
The output is saved under :file:`dist/singlehtml|html|latex`.
"""
import os 
from setuptools import setup, find_packages
from {{cookiecutter.package}}.__meta__ import *

setup(
 # Editorial Information
 name                 = __product__,
 version              = __version__,
 author               = __author__,
 author_email         = __email__,
 url                  = __website__,
 license              = __licence__,
 description          = __summary__,
 long_description     = open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
 long_description_content_type = "text/markdown",
 # Package Information
 platforms            = 'any',
 install_requires     = ["QtPy",],
 packages             = find_packages(exclude = ["build","dist","docs","tests"]),
 test_suite           = "tests",
 tests_require        = [], 
 # Package Operations
 command_options      = {'build_sphinx': {
                          'version'    : ('setup.py', __version__),
                          'release'    : ('setup.py', __release__),
                          'source_dir' : ('setup.py','docs'),
                          'build_dir'  : ('setup.py','dist'),
                          'config_dir' : ('setup.py','docs'),}}, 
 # Meta Information
 classifiers          = [ # See  https://pypi.org/classifiers/
  "Development Status :: 1 - Planning",
  "Framework :: Qt-Twee ",
  "Intended Audience :: End Users/Desktop",
  "License :: OSI Approved :: BSD License",
  "Operating System :: OS Independent",
  "Programming Language :: Python :: 3 :: Only",
  "Topic :: Desktop Environment",
 ],
)
