﻿#!/usr/bin/env python
"""
Meta Data
=========


This file contains the legal and editorial information for the project.
"""
from datetime import datetime

def copyright(company = "", author="", project = "", years = [datetime.now().year]) :
  """Computes the years that the copyright spans"""
  def copyyears(*years) :
    """Converts a set of years into a form suitable for a copyright string"""
    source = sorted(set(years))
    target = []
    tail   = 0
    while tail < len(source) : 
      head = 0
      while tail + head < len(source) and source[tail] + head == source[tail+head]: head += 1
      if head == 1 :
        target.append("{}".format(source[tail]))
      elif head == 2 :
        target.append("{}".format(source[tail]))
        target.append("{}".format(source[tail+head-1]))
      else : 
        target.append("{}-{}".format(source[tail],source[tail+head-1]))
      tail = tail + head  
    return ", ".join(target)
    copyright(__project__,__company__,({{cookiecutter.copyyear}},datetime.now().year))
  if company :
    if project :
      return f"Copyright © {copyyears(*years)}, {company} and the {project} contributors"
    else :
      return f"Copyright © {copyyears(*years)}, {company}"
  return f"Copyright © {copyyears(*years)}"

def version(version = __version__) :
  """Computes the version information as a string"""
  return ".".join(str(i) for i in version)
 
def release(release = __release__) :
  """Computes the release information as a string"""
  return ".".join(str(i) for i in release) 

# Company Information
__company__   =  "{{cookiecutter.company}}"
__website__   =  "{{cookiecutter.website}}"

# Editorial Information
__author__    =  "{{cookiecutter.author}}"
__editor__    =  "{{cookiecutter.editor}}"
__email__     =  "{{cookiecutter.email}}"
__credits__   = ["{{cookiecutter.author}}"]

# Project information
__project__   =  "{{cookiecutter.project}}"
__product__   =  "{{cookiecutter.product}}"
__summary__   =  "{{cookiecutter.summary}}"
__release__   =  ({{cookiecutter.version}})
__version__   =  __release__[:-1]
__release__   =  release()
__version__   =  version()

# Legal Information
__licence__   =  "{{cookiecutter.licence}}"
__copyright__ = copyright(__company__ or __author__, __project__, [{{cookiecutter.copyyear}},datetime.now().year])

__all__ = [# Company Information
           "__company__",
           "__website__",
           # Editorial Information
           "__author__",
           "__editor__",
           "__email__",
           "__credits__",
           # Project information
           "__project__",
           "__product__",
           "__summary__",
           "__release__",
           "__version__",
           # Legal Information
           "__licence__",
           "__copyright__",]
